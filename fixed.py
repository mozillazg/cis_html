# -*- coding: utf-8 -*-

import re
import sys

from bs4 import BeautifulSoup

in_path = sys.argv[1]
out_path = sys.argv[2]
max_page = int(sys.argv[3]) + 3
toc_max = int(sys.argv[4]) + 3

with open(in_path) as fp:
    html = fp.read()
    fixed_content = html

bs = BeautifulSoup(html, 'html5lib')
re_number_start = re.compile(r'^\d+')
re_number_end = re.compile(r'\d+$')
re_dot = re.compile(r'\.{2,}')

final_titles = []


for n in range(3, toc_max):
    div = bs.select_one('#pf%x' % n)
    toc = div.select('.c .t')
    if not list(toc):
        toc = div.select('.pc .t')
    toc_titles = [x.text.strip() for x in toc][2:]
    fixed_titles = []

    pre_title = ''
    for tit in toc_titles:
        tit = re_dot.sub(' ', tit).replace('!', ' ').strip()
        if not re_number_end.search(tit):
            pre_title = '{} {}'.format(pre_title, tit)
            continue

        tit = re_dot.sub(' ', tit).replace('!', ' ')
        tit = '{} {}'.format(pre_title, tit).strip()
        title = tit.split()[0]
        page = int(tit.split()[-1])
        if page > max_page:
            pre_title = tit
            continue

        fixed_titles.append((title, page))
        pre_title = ''

    for rule, page in fixed_titles:
        if not re_number_start.match(rule):
            continue
        final_titles.append((rule, page))


for rule, page in final_titles:
    page += 1
    old_id = 'pf%x' % page
    old_href = '#{}'.format(old_id)
    new_id = 'rule-{}'.format(rule)
    new_href = '#{}'.format(new_id)

    print(repr(rule), repr(page), old_id, old_href, new_id, new_href)

    fixed_content = fixed_content.replace(
            'href="{}"'.format(old_href), 'href="{}"'.format(new_href))
    fixed_content = fixed_content.replace(
            'id="{}"'.format(old_id), 'id="{}"'.format(new_id))

with open(out_path, 'w') as fp:
    fp.write(fixed_content)
