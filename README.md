


```
make build

output=k8s/v1.5.0/ pdf=CIS_Kubernetes_Benchmark_v1.5.0.pdf make run
python fixed.py k8s/v1.5.0/origin.html k8s/v1.5.0/index.html 273 10

output=k8s/v1.4.1/ pdf=CIS_Kubernetes_Benchmark_v1.4.1.pdf make run
python fixed.py k8s/v1.4.1/origin.html k8s/v1.4.1/index.html 249 10

output=k8s/v1.4.0/ pdf=CIS_Kubernetes_Benchmark_v1.4.0.pdf make run
python fixed.py k8s/v1.4.0/origin.html k8s/v1.4.0/index.html 249 10

output=k8s/v1.3.0/ pdf=CIS_Kubernetes_Benchmark_v1.3.0.pdf make run
python fixed.py k8s/v1.3.0/origin.html k8s/v1.3.0/index.html 246 9

output=k8s/v1.2.0/ pdf=CIS_Kubernetes_Benchmark_v1.2.0.pdf make run
python fixed.py k8s/v1.2.0/origin.html k8s/v1.2.0/index.html 273 10

output=k8s/v1.1.0/ pdf=CIS_Kubernetes_Benchmark_v1.1.0.pdf make run
python fixed.py k8s/v1.1.0/origin.html k8s/v1.1.0/index.html 252 10


output=docker/v1.2.0/ pdf=CIS_Docker_Benchmark_v1.2.0.pdf make run
python fixed.py docker/v1.2.0/origin.html docker/v1.2.0/index.html 253 9

output=docker-community-edition/v1.1.0/ pdf=CIS_Docker_Community_Edition_Benchmark_v1.1.0.pdf make run
python fixed.py docker-community-edition/v1.1.0/origin.html docker-community-edition/v1.1.0/index.html 228 9

output=docker-1.13.0/v1.0.0/ pdf=CIS_Docker_1.13.0_Benchmark_v1.0.0.pdf make run
python fixed.py docker-1.13.0/v1.0.0/origin.html docker-1.13.0/v1.0.0/index.html 194 8

output=docker-1.12.0/v1.0.0/ pdf=CIS_Docker_1.12.0_Benchmark_v1.0.0.pdf make run
python fixed.py docker-1.12.0/v1.0.0/origin.html docker-1.12.0/v1.0.0/index.html 185 8

output=docker-1.11.0/v1.0.0/ pdf=CIS_Docker_1.11.0_Benchmark_v1.0.0.pdf make run
python fixed.py docker-1.11.0/v1.0.0/origin.html docker-1.11.0/v1.0.0/index.html 160 7

output=docker-1.6/v1.0.0/ pdf=CIS_Docker_1.6_Benchmark_v1.0.0.pdf make run
python fixed.py docker-1.6/v1.0.0/origin.html docker-1.6/v1.0.0/index.html 118 8


output=linux/v2.0.0/ pdf=CIS_Distribution_Independent_Linux_Benchmark_v2.0.0.pdf make run
python fixed.py linux/v2.0.0/origin.html linux/v2.0.0/index.html 556 13

output=linux/v1.1.0/ pdf=CIS_Distribution_Independent_Linux_Benchmark_v1.1.0.pdf make run
python fixed.py linux/v1.1.0/origin.html linux/v1.1.0/index.html 404 13
```
