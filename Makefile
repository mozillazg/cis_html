build:
	asciidoctor index.asciidoc -o index.html

.PHONY: run
run:
	docker run --rm -v `pwd`:/pdf mozillazg/pdf2htmlex:20191124 pdf2htmlEX \
		--embed cfijo --dest-dir "$(output)" \
		--font-size-multiplier 1 --zoom 1.5 "$(pdf)" origin.html

.PHONY: gen
gen:
	output="$(output)" pdf="$(pdf)" make run
	python fixed.py "$(output)/origin.html" "$(output)/index.html" "$(max_page)" "$(toc_end)"
